# -*- coding: utf-8 -*-

from __future__ import unicode_literals


##
# OAI-PMH repository settings
#
OAI_PMH = {
    'repository_name': "Pod repository",
    'repository_identifier': "your.repo.id",
    'repository_base_url': "http://pod.univ.fr",
    'content_rights': "CC-By-ND-NC",
    'filter_args': {
        'owner_id__is_staff': True,
    },
    # “exclude_arg” MUST contain only ONE element.
    'exclude_arg': {
        'owner_id__username': "root",
        # 'description': "",
    },
    'flow_control': {
        'treshold': 250,
        'ttl': 86400,
    },
    'allow_empty_set_arg': True,
    'private_access_ips': [],
    'log': {
        'is_active': False,
        'file_path': "/path/to/your/file.log",
        'non_logged_ips': [],
    },
}

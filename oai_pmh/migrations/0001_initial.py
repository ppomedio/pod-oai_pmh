# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('pods', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='OAIItem',
            fields=[
            ],
            options={
                'ordering': ('id',),
                'proxy': True,
            },
            bases=('pods.pod',),
        ),
    ]

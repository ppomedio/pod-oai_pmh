# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from django.conf import settings
from bs4 import BeautifulSoup
from settings import OAI_PMH


def get_dcmi_type(media_type, language="eng"):
    dcmi_type = {
        'eng': {
            'audio': "sound",
            'video': "moving image",
        },
        'fre': {
            'audio': "son",
            'video': "image en mouvement",
        },
    }
    return dcmi_type[language][media_type]


def get_xml_text(chicken_soup):
    vegetable_soup = BeautifulSoup(chicken_soup, "html.parser").get_text()
    return vegetable_soup.strip(" \t\n\r").replace(
        "&", "&amp;").replace("<", "&lt;").replace(">", "&gt;")


def get_admins_email():
    admins_email = ()
    if settings.ADMINS:
        for admin in settings.ADMINS:
            admins_email += (admin[1],)
    return admins_email


POD_OAI = {
    'admins_email': get_admins_email(),
    'title_etb': get_xml_text(
        settings.TITLE_ETB) if settings.TITLE_ETB else "",
    'repository_name': get_xml_text(
            OAI_PMH['repository_name']
        ) if OAI_PMH['repository_name'] else "",
}

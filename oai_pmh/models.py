# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from django.utils.encoding import python_2_unicode_compatible
from pods.models import Channel, Discipline, Pod, Theme, Type
from iso639 import languages
from settings import OAI_PMH
from tools import *


@python_2_unicode_compatible
class OAIItem(Pod):

    class Meta:
        proxy = True
        ordering = ('id', )

    def __str__(self):
        return "%s - %s" % ("%04d" % self.id, self.title)

    def __build_url(self):
        return "%s/video/%s/" % (
                OAI_PMH['repository_base_url'],
                self.slug
            )

    def __get_oai_dc(self):

        def get_subjects():
            subjects = []
            for discipline in self.discipline.all().values_list(
                    'title', flat=True):
                subjects.append(get_xml_text(discipline))
            for keyword in self.tags.all().values_list('name', flat=True):
                subjects.append(get_xml_text(keyword))
            return subjects

        def get_creator():
            if self.owner.last_name and self.owner.first_name:
                return "%s %s" % (
                    self.owner.last_name.upper(), self.owner.first_name)
            else:
                return self.owner.username

        return {
                'dc_title': get_xml_text(self.title),
                'dc_creator': get_creator(),
                'dc_subject': get_subjects(),
                'dc_description': get_xml_text(self.description),
                'dc_publisher': POD_OAI['title_etb'],
                'dc_contributor': self.contributorpods_set.values_list(
                    'name', flat=True),
                'dc_date': self.date_added,
                'dc_type': get_dcmi_type(self.get_mediatype()[0]),
                'dc_identifier': self._OAIItem__build_url(),
                'dc_language': languages.get(
                    alpha2=self.main_lang).bibliographic,
                'dc_rights': OAI_PMH['content_rights'],
            }

    def __get_lom(self):
        import time

        def build_vcard(contributor=None, main_org=False):
            if (main_org is False and contributor is None):
                if self.owner.last_name and self.owner.first_name:
                    return ("BEGIN:VCARD VERSION:3.0 N:%s;%s;;; "
                        "FN:%s END:VCARD" % (
                            self.owner.last_name.upper(),
                            self.owner.first_name,
                            "%s %s" % (
                                self.owner.first_name,
                                self.owner.last_name.upper()),
                        ))
                else:
                    return ("BEGIN:VCARD VERSION:3.0 N:%s;;;; "
                        "FN:%s END:VCARD" % (
                            self.owner.username,
                            self.owner.username,
                        ))
            elif contributor is not None:
                return "BEGIN:VCARD VERSION:3.0 N:;;;; FN:%s END:VCARD" % (
                        contributor
                    )
            else:
                return ("BEGIN:VCARD VERSION:3.0 N:;;;; "
                        "FN:%s ORG:%s END:VCARD" % (
                            POD_OAI['title_etb'],
                            POD_OAI['title_etb'],
                        ))

        def get_contributors():
            contributors = self.contributorpods_set.values_list(
                'name', flat=True)
            lom_contributors = ()
            for contributor in contributors:
                lom_contributors += ({
                    'role': {
                        'value': "contributeur",
                    },
                    'entity': build_vcard(contributor=contributor),
                    'date': self.date_added,
                },)
            return lom_contributors

        def get_educational_context():
            cursus_codes = {
                'L': {
                    'source': "LOMFRv1.0",
                    'value': "licence",
                },
                'M': {
                    'source': "LOMFRv1.0",
                    'value': "master",
                },
                'D': {
                    'source': "LOMFRv1.0",
                    'value': "doctorat",
                },
                '1': {
                    'source': "LOMv1.0",
                    'value': "other",
                },
            }
            if self.cursus in cursus_codes:
                return (cursus_codes[self.cursus],)
            else:
                return (cursus_codes['1'],)

        def get_keywords():
            keywords = self.tags.all().values_list('name', flat=True)
            lom_keywords = ()
            for keyword in keywords:
                lom_keywords += ({
                    # 'lang': "und",
                    'lang': "fre",
                    'data': get_xml_text(keyword),
                },)
            return lom_keywords

        return {
                'general': {
                    'identifier': {
                        'entry': self._OAIItem__build_url(),
                    },
                    'title': {
                        # 'lang': "und",
                        'lang': "fre",
                        'data': get_xml_text(self.title),
                    },
                    'language': languages.get(
                            alpha2=self.main_lang).bibliographic,
                    'description': {
                        # 'lang': "und",
                        'lang': "fre",
                        'data': get_xml_text(self.description),
                    },
                    'keyword': get_keywords(),
                    'document_type': get_dcmi_type(
                            self.get_mediatype()[0], "fre"),
                },
                'life_cycle': {
                    'contribute': (
                        {
                            'role': {
                                # 'value': "auteur",
                                'value': "fournisseur de contenus",
                            },
                            'entity': build_vcard(),
                            'date': self.date_added,
                        },
                        {
                            'role': {
                                'value': "éditeur",
                            },
                            'entity': build_vcard(main_org=True),
                            'date': self.date_added,
                        },
                    ) + get_contributors(),
                },
                'technical': {
                    'location': self._OAIItem__build_url(),
                    'duration': time.strftime(
                        "PT%-HH%-MM%-SS", time.gmtime(self.duration)),
                },
                'educational': {
                    'context': (
                        {
                            'source': "LOMv1.0",
                            'value': "higher education",
                        },
                    ) + get_educational_context(),
                },
                'rights': {
                    'description': {
                        'lang': "fre",
                        'data': "Cette ressource est mise à disposition "
                                "sous un contrat Creative Commons "
                                "%s." % OAI_PMH['content_rights'],
                    }
                },
            }

    def build_identifier(self):
        return "oai:%s:%s" % (
                OAI_PMH['repository_identifier'],
                "%04d" % self.id
            )

    def get_metadata(self, metadataPrefix, header_only=False):
        set_specs = ("T%s" % self.type.id, )
        __ = Channel.objects.filter(**{'pod__id': self.id}).values()
        for item in __:
            set_specs += ("%s" % item['id'], )
        __ = Theme.objects.filter(**{'pod__id': self.id}).values()
        for item in __:
            set_specs += ("%s:%s" % (item['channel_id'], item['id']), )
        __ = Discipline.objects.filter(**{'pod__id': self.id}).values()
        for item in __:
            set_specs += ("D%s" % item['id'], )
        metadata = {
            'header': {
                'identifier': self.build_identifier(),
                'datestamp': self.last_modified,
                'setSpecs': set_specs,
            },
        }
        if not header_only:
            metadata[metadataPrefix] = getattr(
                self, "_OAIItem__get_%s" % metadataPrefix)()
        return metadata

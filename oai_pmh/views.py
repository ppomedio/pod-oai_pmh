# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from django.core.cache import caches
from django.core.paginator import Paginator, InvalidPage
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import TemplateView

from datetime import datetime, date, timedelta

from pods.models import Channel, Discipline, Theme, Type
from oai_pmh.models import OAIItem
from settings import OAI_PMH
from tools import get_xml_text, POD_OAI


class OAIRepository(TemplateView):

    oai_resumption_metadata_prefix = None
    oai_request_verb = None
    oai_request_args = None
    oai_pmh_cache = None

    oai_metadata_formats = {
        'oai_dc': {
            'schema': "http://www.openarchives.org/OAI/2.0/oai_dc.xsd",
            'namespace': "http://www.openarchives.org/OAI/2.0/oai_dc/",
        },
        'lom': {
            'schema': "http://ltsc.ieee.org/xsd/lomv1.0/lom.xsd",
            'namespace': "http://ltsc.ieee.org/xsd/LOM",
        },
    }

    OAI_PMH['filter_args'].update({
        'is_restricted': False,
        'is_draft': False,
        'password': "",
        'to_encode': False,
        'encoding_in_progress': False,
    })

    def __oai_verb_get_record(self):
        # http://www.openarchives.org/OAI/openarchivesprotocol.html#GetRecord
        args_len = len(self.oai_request_args.keys())
        if (self.oai_request_args.get('identifier', None) and
                self.oai_request_args.get('metadataPrefix', None) and
                args_len == 2):
            if (self.oai_request_args['metadataPrefix'] in
                    self.oai_metadata_formats):
                result = self._OAIRepository__get_item_by_identifier()
                if result[0] is not None:
                    return self._OAIRepository__error(result[0])
            else:
                return self._OAIRepository__error('cannotDisseminateFormat')
        else:
            return self._OAIRepository__error('badArgument')
        items = {
            'records': [result[1][0].get_metadata(
                    self.oai_request_args['metadataPrefix']), ]
        }
        self.template_name = "record-%s.xml" % (
                self.oai_request_args['metadataPrefix'])
        return self.render_to_response(items)

    def __oai_verb_identify(self):
        # http://www.openarchives.org/OAI/openarchivesprotocol.html#Identify
        if self.oai_request_args:
            return self._OAIRepository__error('badArgument')
        items = {
            'repository_name': ("%s - %s" % (
                POD_OAI['repository_name'],
                POD_OAI['title_etb'],
                )),
            'admins_email': POD_OAI['admins_email'],
            'deleted': "no",
            'granularity': "YYYY-MM-DD",
            # 'compression': "gzip/compress/deflate - optional",
        }
        try:
            earliest_item = OAIItem.objects.filter(
                **OAI_PMH['filter_args']).exclude(
                **OAI_PMH['exclude_arg']).order_by('last_modified')[0]
        except IndexError:
            items['earliest_date'] = date.today()
            items['description'] = None
        else:
            items['earliest_date'] = earliest_item.last_modified
            items['description'] = {
                'identifier_scheme': "oai",
                'repository_identifier': OAI_PMH['repository_identifier'],
                'identifier_delimiter': ":",
                'sample_identifier': earliest_item.build_identifier()
            }
        self.template_name = "identify.xml"
        return self.render_to_response(items)

    def __oai_verb_list_identifiers(self):
        # http://www.openarchives.org/OAI/openarchivesprotocol.html#ListIdentifiers
        result = self._OAIRepository__get_items()
        if result[0] is not None:
            return self._OAIRepository__error(result[0])
        self.template_name = "list_identifiers.xml"
        return self.render_to_response(result[1])

    def __oai_verb_list_metadata_formats(self):
        # http://www.openarchives.org/OAI/openarchivesprotocol.html#ListMetadataFormats
        args_len = len(self.oai_request_args.keys())
        if self.oai_request_args.get('identifier', None) and args_len == 1:
            result = self._OAIRepository__get_item_by_identifier()
            if result[0] is not None:
                return self._OAIRepository__error(result[0])
        elif args_len > 0:
            return self._OAIRepository__error('badArgument')
        items = {
            'formats': self.oai_metadata_formats
        }
        self.template_name = "list_metadata_formats.xml"
        return self.render_to_response(items)

    def __oai_verb_list_records(self):
        # http://www.openarchives.org/OAI/openarchivesprotocol.html#ListRecords
        result = self._OAIRepository__get_items()
        if result[0] is not None:
            return self._OAIRepository__error(result[0])
        self.template_name = "record-%s.xml" % (
                self.oai_resumption_metadata_prefix)
        return self.render_to_response(result[1])

    def __oai_verb_list_sets(self):
        # http://www.openarchives.org/OAI/openarchivesprotocol.html#ListSets
        if len(self.oai_request_args.keys()) > 0:
            if 'resumptionToken' in self.oai_request_args:
                return self._OAIRepository__error('badResumptionToken')
            else:
                return self._OAIRepository__error('badArgument')
        items = {'sets': (), }
        disciplines = Discipline.objects.all().values()
        for __ in disciplines:
            items['sets'] += (("D%s" % __['id'], get_xml_text(__['title'])),)
        channels = Channel.objects.filter(visible=True).values()
        for __ in channels:
            items['sets'] += (("%s" % __['id'], get_xml_text(__['title'])),)
            themes = Theme.objects.filter(channel_id=__['id']).values()
            channel_id = __['id']
            for __ in themes:
                items['sets'] += (("%s:%s" % (
                        channel_id, __['id']), get_xml_text(__['title'])),)
        types = Type.objects.all().values()
        for __ in types:
            items['sets'] += (("T%s" % __['id'], get_xml_text(__['title'])),)
        if not items['sets']:
            return self._OAIRepository__error('noSetHierarchy')
        self.template_name = "list_sets.xml"
        return self.render_to_response(items)

    def __error(self, error_code):
        # http://www.openarchives.org/OAI/2.0/guidelines-repository.htm#ErrorHandling
        # http://www.openarchives.org/OAI/openarchivesprotocol.html#ErrorConditions
        oai_errors = {
            'badArgument': ("The request includes illegal arguments, is "
                            "missing required arguments, includes a repeated "
                            "argument, or values for arguments have an "
                            "illegal syntax."),
            'badResumptionToken': ("The value of the resumptionToken "
                                   "argument is invalid or expired."),
            'badVerb': ("Value of the verb argument is not a legal OAI-PMH "
                        "verb, the verb argument is missing, or the verb "
                        "argument is repeated."),
            'cannotDisseminateFormat': ("The metadata format identified by "
                                        "the value given for the "
                                        "metadataPrefix argument is not "
                                        "supported by the item or by the "
                                        "repository."),
            'idDoesNotExist': ("The value of the identifier argument is "
                               "unknown or illegal in this repository."),
            'noRecordsMatch': ("The combination of the values of the from, "
                               "until, set and metadataPrefix arguments "
                               "results in an empty list."),
            'noMetadataFormats': ("There are no metadata formats available "
                                  "for the specified item."),
            'noSetHierarchy': "The repository does not support sets.",
        }
        self.template_name = "error.xml"
        return self.render_to_response({
                'error_code': error_code,
                'error_description': oai_errors[error_code],
            })

    def __get_item_by_identifier(self):
        from django.core.exceptions import ObjectDoesNotExist
        if not self.oai_request_args['identifier'].startswith(
                "oai:%s:" % OAI_PMH['repository_identifier']):
            return ('badArgument', None)
        try:
            item_id = int(self.oai_request_args['identifier'].replace(
                    "oai:%s:" % OAI_PMH['repository_identifier'], ""))
        except ValueError:
            return ('badArgument', None)
        try:
            item = OAIItem.objects.get(**{'id': item_id})
        except ObjectDoesNotExist:
            return ('idDoesNotExist', None)
        filter_args = OAI_PMH['filter_args'].copy()
        filter_args['id'] = item_id
        item = OAIItem.objects.filter(
            **filter_args).exclude(**OAI_PMH['exclude_arg'])
        if not item:
            return ('noMetadataFormats', None)
        return (None, item)

    def __get_items(self):
        # http://www.openarchives.org/OAI/openarchivesprotocol.html#FlowControl
        # http://www.openarchives.org/OAI/2.0/guidelines-repository.htm#resumptionToken
        def flow_init(cache_method, dataset):
            oai_request_ttl = (datetime.utcnow() + timedelta(**{
                    'seconds': OAI_PMH['flow_control']['ttl']
                })).strftime("%Y-%m-%dT%H:%M:%SZ")
            getattr(self.oai_pmh_cache, cache_method)(
                    oai_request_key,
                    (oai_request_ttl, dataset),
                    OAI_PMH['flow_control']['ttl']
                )
            paginator = Paginator(
                    dataset, OAI_PMH['flow_control']['treshold'])
            return (
                    paginator.page(1).object_list,
                    {
                        'key': "%s|%s|1" % (
                            oai_request_key,
                            self.oai_resumption_metadata_prefix
                        ),
                        'args': {
                            'expiration_date': oai_request_ttl,
                            'complete_list_size': paginator.count,
                            'cursor': 0,
                        },
                    }
                )

        items = {}
        if 'resumptionToken' in self.oai_request_args:
            # Processing resumptionToken
            if (self.oai_request_args['resumptionToken'] == ""
                    or len(self.oai_request_args.keys()) > 1):
                return ('badArgument', None)
            else:
                __ = self.oai_request_args['resumptionToken'].split("|")
                try:
                    oai_request_key = __[0]
                    self.oai_resumption_metadata_prefix = __[1]
                    oai_token_page = __[2]
                except IndexError:
                    return ('badResumptionToken', None)
                if (self.oai_resumption_metadata_prefix
                        not in self.oai_metadata_formats):
                    return ('badResumptionToken', None)
                oai_request_value = self.oai_pmh_cache.get(
                        oai_request_key)
                if oai_request_value is None:
                    return ('badResumptionToken', None)
                paginator = Paginator(
                        oai_request_value[1],
                        OAI_PMH['flow_control']['treshold']
                    )
                try:
                    page = paginator.page(oai_token_page)
                except InvalidPage:
                    return ('badResumptionToken', None)
                if page.has_next():
                    oai_token_page = page.next_page_number()
                    items['resumption_token'] = {
                        'key': "",
                        'args': {
                            'complete_list_size': paginator.count,
                            'cursor': paginator.page(
                                    oai_token_page).start_index() - 1,
                        },
                    }
                    if paginator.page(oai_token_page).has_next():
                        items['resumption_token']['key'] = ("%s|%s|%s" % (
                                oai_request_key,
                                self.oai_resumption_metadata_prefix,
                                oai_token_page
                            ))
                        items['resumption_token']['args'][
                                'expiration_date'] = oai_request_value[0]
                    result = (None, paginator.page(
                            oai_token_page).object_list)
                else:
                    return ('badResumptionToken', None)
        else:
            # Request validation
            test_date = {}
            for key, value in self.oai_request_args.items():
                if key not in (
                        'set',
                        'from',
                        'until',
                        'metadataPrefix',
                        'resumptionToken'):
                    return ('badArgument', None)
                if key == "from" or key == "until":
                    try:
                        date_elements = value.split("-")
                        if (len(date_elements) != 3 or
                                len(date_elements[0]) != 4 or
                                len(date_elements[1]) != 2 or
                                len(date_elements[2]) != 2):
                            raise ValueError()
                        test_date[key] = datetime.strptime(
                                value, "%Y-%m-%d").date()
                    except ValueError:
                        return ('badArgument', None)
                if len(test_date) == 2:
                    if not test_date["from"] <= test_date["until"]:
                        return ('badArgument', None)
            if self.oai_request_args.get('metadataPrefix', None):
                if (self.oai_request_args['metadataPrefix'] not in
                        self.oai_metadata_formats):
                    return ('cannotDisseminateFormat', None)
            else:
                return ('badArgument', None)
            # Initialization
            self.oai_resumption_metadata_prefix = (
                    self.oai_request_args['metadataPrefix'])
            oai_request_key = ("%s%s%s" % (
                    self.oai_request_args.get('from', None),
                    self.oai_request_args.get('until', None),
                    self.oai_request_args.get('set', None),
                )).replace("-", "")
            oai_request_value = self.oai_pmh_cache.get(
                    oai_request_key)
            if oai_request_value is None:
                # Building request
                filter_args = OAI_PMH['filter_args'].copy()
                date_range = {
                    'from': None,
                    'until': None,
                }
                if 'from' in self.oai_request_args:
                    date_range['from'] = self.oai_request_args['from']
                if 'until' in self.oai_request_args:
                    date_range['until'] = self.oai_request_args['until']
                if date_range['from'] and date_range['until']:
                    filter_args['last_modified__range'] = (
                        date_range['from'],
                        date_range['until']
                    )
                elif date_range['from']:
                    filter_args['last_modified__gte'] = date_range['from']
                elif date_range['until']:
                    filter_args['last_modified__lte'] = date_range['until']
                if self.oai_request_args.get('set', None):
                    __ = self.oai_request_args['set']
                    if __[0] == "T":
                        filter_args['type_id'] = __[1:]
                    elif __[0] == "D":
                        filter_args['discipline__id'] = __[1:]
                    else:
                        ___ = __.split(":")
                        try:
                            if ___[1]:
                                filter_args['theme__id'] = ___[1]
                                filter_args['theme__channel_id'] = ___[0]
                            else:
                                filter_args['channel__id'] = __
                        except IndexError:
                            filter_args['channel__id'] = __
                # Getting related items
                try:
                    result = (None, OAIItem.objects.filter(
                        **filter_args).exclude(**OAI_PMH['exclude_arg']))
                except ValueError:
                    return ('noRecordsMatch', None)
                if not result[1]:
                    return ('noRecordsMatch', None)
                if len(result[1]) > OAI_PMH['flow_control']['treshold']:
                    flow_element = flow_init("add", result[1])
                    result = (None, flow_element[0])
                    items['resumption_token'] = flow_element[1]
            else:
                flow_element = flow_init("set", oai_request_value[1])
                result = (None, flow_element[0])
                items['resumption_token'] = flow_element[1]
        items['records'] = []
        for item in result[1]:
            items['records'].append(item.get_metadata(
                    self.oai_resumption_metadata_prefix))
        return (result[0], items)

    def __process_request(self, request):
        # Harvesting request logging
        if (OAI_PMH['log']['is_active'] and
                request.META['REMOTE_ADDR'] not in
                OAI_PMH['log']['non_logged_ips']):
            log_file = open(OAI_PMH['log']['file_path'], "a")
            if request.method == "GET":
                query_items = request.GET.lists()
            elif request.method == "POST":
                query_items = request.POST.lists()
            else:
                query_items = None
            query_params = " "
            if query_items is not None:
                for key, values in query_items:
                    for value in values:
                        query_params = "%s%s=%s " % (
                                query_params,
                                key,
                                value
                        )
            log_file.write("%s | %s | %s |%s| %s\n" % (
                datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
                request.META['REMOTE_ADDR'],
                request.method,
                query_params,
                request.META['HTTP_USER_AGENT']
            ))
            log_file.close()
        # OAI-PMH request basic validation
        for key, value in self.oai_request_args.lists():
            if len(value) > 1:
                if key == 'verb':
                    return self._OAIRepository__error('badVerb')
                else:
                    return self._OAIRepository__error('badArgument')
            else:
                if not value[0]:
                    if not (OAI_PMH['allow_empty_set_arg'] and key == "set"):
                        return self._OAIRepository__error('badArgument')
        self.oai_request_verb = self.oai_request_args.get('verb', None)
        oai_verbs = {
            'GetRecord': "get_record",
            'Identify': "identify",
            'ListIdentifiers': "list_identifiers",
            'ListMetadataFormats': "list_metadata_formats",
            'ListRecords': "list_records",
            'ListSets': "list_sets",
        }
        if self.oai_request_verb not in oai_verbs:
            return self._OAIRepository__error('badVerb')
        self.oai_request_args.pop('verb')
        for arg in self.oai_request_args:
            if arg not in (
                    'set',
                    'from',
                    'until',
                    'identifier',
                    'metadataPrefix',
                    'resumptionToken'):
                return self._OAIRepository__error('badArgument')
        # Publish restricted access content for authorized IPs
        if request.META['REMOTE_ADDR'] in OAI_PMH['private_access_ips']:
            self.oai_pmh_cache = caches['oai_pmh_private_cache']
            OAI_PMH['filter_args'].pop('is_restricted', 'None')
        else:
            self.oai_pmh_cache = caches['oai_pmh_public_cache']
        # Basic validation ok, dispatching to verb processing method
        return getattr(self, "_OAIRepository__oai_verb_%s" % (
                oai_verbs[self.oai_request_verb]
            ))()

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(OAIRepository, self).dispatch(*args, **kwargs)

    def get(self, request):
        self.oai_request_args = request.GET.copy()
        return self._OAIRepository__process_request(request)

    def post(self, request):
        self.oai_request_args = request.POST.copy()
        return self._OAIRepository__process_request(request)

    def render_to_response(self, context, **response_kwargs):
        response_kwargs['content_type'] = "text/xml; charset=UTF-8"
        context.update({
            'oai_url': "%s/oai2/" % OAI_PMH['repository_base_url'],
            'response_date': datetime.utcnow().strftime("%Y-%m-%dT%H:%M:%SZ")
        })
        if not ('error_code' in context and (
                context['error_code'] == "badVerb" or
                context['error_code'] == "badArgument")):
            context.update({
                'verb': self.oai_request_verb,
                'args': self.oai_request_args,
            })
        return super(TemplateView, self).render_to_response(
                context, **response_kwargs)

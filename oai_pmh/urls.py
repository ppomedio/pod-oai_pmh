# -*- coding: utf-8 -*-

from django.conf.urls import url
from oai_pmh.views import OAIRepository


urlpatterns = [
    url(r'^$', OAIRepository.as_view()),
]
